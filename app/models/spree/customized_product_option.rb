module Spree
  # in populate, params[:customization] contains all the fields supplied by
  # the customization_type_view. Those values are saved in this class
  class CustomizedProductOption < ActiveRecord::Base
    belongs_to :product_customization
    belongs_to :customizable_product_option

    has_attached_file :customization_image,
                      styles: {
                        thumb: '50x50^'
                      },
                      convert_options: {
                        thumb: " -gravity center -crop '50x50+0+0'",
                      }

    def empty?
      value.empty? && !customization_image?
    end
  end
end
